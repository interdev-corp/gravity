package com.interdev.gravity.level;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.interdev.gravity.entity.component.PhysicsComponent;
import com.interdev.gravity.entity.model.HangarModel;
import com.interdev.gravity.entity.model.PlanetModel;
import com.interdev.gravity.entity.model.SpaceShipModel;
import com.interdev.gravity.entity.model.WorldModel;

public class FirstLevel extends AbstractLevel {

	private World world;

	private Entity sun;
	private Entity planet;
	private Entity ship;
	private Entity hangar;

	public FirstLevel(InputMultiplexer inputMultiplexer) {
		world = new World(Vector2.Zero, true);
		addBackground();
		addHUD(inputMultiplexer);

		Vector2 worldCenter = new Vector2(WorldModel.WORLD_WIDTH / 2, WorldModel.WORLD_HEIGHT / 2);
		Texture sunT = new Texture(Gdx.files.internal("textures/sun.png"));
		sunT.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
		sun = new PlanetModel.Builder()
				.withTexture(sunT)
				.withPhysics(world, worldCenter, 20).withGravityFieldRadius(90).fixed(true).build();
		planet = new PlanetModel.Builder()
				.withTexture(new Texture(Gdx.files.internal("textures/planet.png")))
				.withPhysics(world, worldCenter.sub(35, 0), 8).withGravityFieldRadius(50).build();
		ship = new SpaceShipModel.Builder().withTexture().withPhysics(world, new Vector2(WorldModel.WORLD_WIDTH / 2, 10))
				.withFuel(100).withGravity(10).withParticleEffect().build();
		hangar = new HangarModel.Builder().withTexture(new Texture(Gdx.files.internal("textures/hangar.png")))
				.withPhysics(world, new Vector2(WorldModel.WORLD_WIDTH / 2, WorldModel.WORLD_HEIGHT - 20)).build();
		setInitialPhysicsState();

		entities.add(sun);
		entities.add(planet);
		entities.add(ship);
		entities.add(hangar);
	}

	private void setInitialPhysicsState() {
		Body planetBody = planet.getComponent(PhysicsComponent.class).body;
		planetBody.setLinearVelocity(new Vector2(0, 22));
	}

	@Override
	public World getGameWorld() {
		return world;
	}
}
