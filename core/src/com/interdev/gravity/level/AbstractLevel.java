package com.interdev.gravity.level;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.InputMultiplexer;
import com.interdev.gravity.entity.model.BackgroundModel;
import com.interdev.gravity.entity.model.HUDModel;

import java.util.ArrayList;
import java.util.List;

abstract class AbstractLevel implements GameLevel {

	protected List<Entity> entities = new ArrayList<>();

	@Override
	public void registerEntities(Engine engine) {
		for (Entity entity : entities) {
			engine.addEntity(entity);
		}
	}

	protected void addHUD(InputMultiplexer inputMultiplexer) {
		Entity hudEntity = new HUDModel.Builder().init(inputMultiplexer).withProgressBar().withVirtualJoystick().build();
		entities.add(hudEntity);
	}

	protected void addBackground() {
		Entity background = new BackgroundModel.Builder().init().build();
		entities.add(background);
	}
}
