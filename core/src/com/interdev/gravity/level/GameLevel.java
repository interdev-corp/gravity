package com.interdev.gravity.level;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.physics.box2d.World;

public interface GameLevel {

	public void registerEntities(Engine engine);

	public World getGameWorld();

}
