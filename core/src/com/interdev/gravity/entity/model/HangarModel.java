package com.interdev.gravity.entity.model;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.interdev.gravity.entity.component.PhysicsComponent;
import com.interdev.gravity.entity.component.SpriteComponent;

public class HangarModel extends AbstractModel {

	private static final float HANGAR_WIDTH = 40;

	private PhysicsComponent physicsComponent = new PhysicsComponent();
	private SpriteComponent spriteComponent = new SpriteComponent();

	private HangarModel() {
		addComponents(physicsComponent, spriteComponent);
	}

	public static class Builder implements EntityBuilder {

		private HangarModel hangar = new HangarModel();
		private Sprite sprite;

		public Builder() {
			sprite = new Sprite();
		}

		public HangarModel.Builder withTexture(Texture texture) {
			sprite = new Sprite(texture);
			float ratio = texture.getHeight() / (float) texture.getWidth();
			sprite.setSize(HANGAR_WIDTH, ratio * HANGAR_WIDTH);
			hangar.spriteComponent.sprite = sprite;
			return this;
		}

		public HangarModel.Builder withPhysics(World world, Vector2 position) {
			BodyDef bodyDef = new BodyDef();
			bodyDef.type = BodyDef.BodyType.StaticBody;
			bodyDef.position.set(position);

			Body body = world.createBody(bodyDef);
			PolygonShape polygonShape = new PolygonShape();
			polygonShape.setAsBox(sprite.getWidth() / 2, sprite.getHeight() / 2);

			FixtureDef fixtureDef = new FixtureDef();
			fixtureDef.shape = polygonShape;

			body.createFixture(fixtureDef);
			hangar.physicsComponent.body = body;
			body.setUserData(HangarModel.class);
			return this;
		}

		@Override
		public Entity build() {
			return hangar.createEntity();
		}
	}
}
