package com.interdev.gravity.entity.model;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.interdev.gravity.entity.component.PhysicsComponent;
import com.interdev.gravity.entity.component.PlanetaryGravityComponent;
import com.interdev.gravity.entity.component.SpriteComponent;

public final class PlanetModel extends AbstractModel {

	private PhysicsComponent physicsComponent = new PhysicsComponent();
	private SpriteComponent spriteComponent = new SpriteComponent();
	private PlanetaryGravityComponent planetaryGravityComponent = new PlanetaryGravityComponent();

	private PlanetModel() {
		addComponents(physicsComponent, spriteComponent, planetaryGravityComponent);
	}

	public static class Builder implements EntityBuilder {

		private PlanetModel planet = new PlanetModel();
		private float radius = 0;

		public Builder() {
			planet.spriteComponent.sprite = new Sprite();
		}

		public Builder withTexture(Texture texture) {
			Sprite sprite = new Sprite(texture);
			sprite.setSize(radius * 2, radius * 2);
			planet.spriteComponent.sprite = sprite;
			return this;
		}

		public Builder withPhysics(World world, Vector2 centerPosition, float radius) {
			BodyDef bodyDef = new BodyDef();
			bodyDef.type = BodyDef.BodyType.DynamicBody;
			bodyDef.position.set(centerPosition);

			Body body = world.createBody(bodyDef);

			CircleShape circle = new CircleShape();
			circle.setRadius(radius);

			FixtureDef fixtureDef = new FixtureDef();
			fixtureDef.shape = circle;

			body.createFixture(fixtureDef);
			planet.physicsComponent.body = body;
			this.radius = radius;
			if (planet.spriteComponent.sprite != null) {
				planet.spriteComponent.sprite.setSize(radius * 2, radius * 2);
			}
			body.setUserData(PlanetModel.class);
			return this;
		}

		public Builder withGravityFieldRadius(float radius) {
			planet.planetaryGravityComponent.gravityFieldRadius = radius;
			return this;
		}

		public Builder fixed(boolean fixed) {
			planet.planetaryGravityComponent.fixed = fixed;
			return this;
		}

		@Override
		public Entity build() {
			return planet.createEntity();
		}
	}

}
