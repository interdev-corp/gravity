package com.interdev.gravity.entity.model;


import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.interdev.gravity.entity.component.*;

public class SpaceShipModel extends AbstractModel {

	private static final String SHIP_TEXTURE_PATH = "textures/rocket.png";
	private static final String SHIP_ROCKET_EFFECT_PATH = "particles/particles.p";
	private static final String SHIP_ROCKET_EFFECT_IMAGES_PATH = "particles";
	private static final float SHIP_WIDTH = 12;
	;

	private PhysicsComponent physicsComponent = new PhysicsComponent();
	private SpriteComponent spriteComponent = new SpriteComponent();
	private SpaceShipComponent spaceShipComponent = new SpaceShipComponent();
	private PlanetaryGravityComponent planetaryGravityComponent = new PlanetaryGravityComponent();
	private ParticleComponent particleComponent = new ParticleComponent();

	private SpaceShipModel() {
		addComponents(physicsComponent, spriteComponent, spaceShipComponent, planetaryGravityComponent, particleComponent);
	}

	public static class Builder {

		private SpaceShipModel spaceShipModel = new SpaceShipModel();
		private Sprite sprite;

		public Builder withTexture() {
			Texture texture = new Texture(Gdx.files.internal(SHIP_TEXTURE_PATH));
			float ratio = texture.getHeight() / (float) texture.getWidth();
			sprite = new Sprite(texture);
			sprite.setSize(SHIP_WIDTH, ratio * SHIP_WIDTH);
			sprite.setOriginCenter();
			spaceShipModel.spriteComponent.sprite = sprite;
			return this;
		}

		public Builder withPhysics(World world, Vector2 centerPosition) {
			BodyDef bodyDef = new BodyDef();
			bodyDef.type = BodyDef.BodyType.DynamicBody;
			bodyDef.position.set(centerPosition);

			Body body = world.createBody(bodyDef);
			PolygonShape polygonShape = new PolygonShape();
			polygonShape.setAsBox(sprite.getWidth() / 2, sprite.getHeight() / 2);

			FixtureDef fixtureDef = new FixtureDef();
			fixtureDef.shape = polygonShape;

			body.createFixture(fixtureDef);
			body.setTransform(body.getPosition(), MathUtils.degreesToRadians * 90);
			spaceShipModel.physicsComponent.body = body;
			body.setUserData(SpaceShipModel.class);
			return this;
		}

		public Builder withGravity(float gravity) {
			spaceShipModel.planetaryGravityComponent.gravityFieldRadius = 10;
			return this;
		}

		public Builder withParticleEffect() {
			ParticleEffect particleEffect = new ParticleEffect();
			particleEffect.load(Gdx.files.internal(SHIP_ROCKET_EFFECT_PATH), Gdx.files.internal(SHIP_ROCKET_EFFECT_IMAGES_PATH));
			particleEffect.getEmitters().first().getLife().setHigh(100);
			particleEffect.start();
			spaceShipModel.particleComponent.particleEffect = particleEffect;
			return this;
		}

		public Builder withFuel(float fuel) {
			spaceShipModel.spaceShipComponent.fuel = fuel;
			return this;
		}

		public Entity build() {
			return spaceShipModel.createEntity();
		}

	}

}
