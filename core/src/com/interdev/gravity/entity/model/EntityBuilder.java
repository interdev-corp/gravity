package com.interdev.gravity.entity.model;

import com.badlogic.ashley.core.Entity;

interface EntityBuilder {

	public Entity build();
}
