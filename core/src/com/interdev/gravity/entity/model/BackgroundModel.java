package com.interdev.gravity.entity.model;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.interdev.gravity.entity.component.BackgroundComponent;

public class BackgroundModel extends AbstractModel {

	private BackgroundComponent backgroundComponent = new BackgroundComponent();

	private BackgroundModel() {
		addComponents(backgroundComponent);
	}

	public static class Builder implements EntityBuilder {

		private BackgroundModel background = new BackgroundModel();

		public Builder init() {
			background.backgroundComponent.texture = new Texture(Gdx.files.internal("textures/space_background.jpg"));
			return this;
		}

		@Override
		public Entity build() {
			return background.createEntity();
		}
	}
}
