package com.interdev.gravity.entity.model;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AbstractModel {

	private List<Component> components = new ArrayList<>();

	public void addComponents(Component... components) {
		this.components.addAll(Arrays.asList(components));

	}

	public Entity createEntity() {
		Entity entity = new Entity();
		for (Component component : components) {
			entity.add(component);
		}
		return entity;
	}

	public void addToEngine(Engine engine) {
		engine.addEntity(createEntity());
	}

}
