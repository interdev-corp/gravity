package com.interdev.gravity.entity.model;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.interdev.gravity.System;
import com.interdev.gravity.entity.component.HUDComponent;

public class HUDModel extends AbstractModel {

	private HUDComponent hudComponent = new HUDComponent();

	private HUDModel() {
		addComponents(hudComponent);
	}

	public static class Builder implements EntityBuilder {

		private static final int PROGRESSBAR_WIDTH = 20;

		private HUDModel hudModel = new HUDModel();
		private Stage stage;
		private Table table;

		public Builder init(InputMultiplexer inputMultiplexer) {
			stage = new Stage(new ScreenViewport());
			inputMultiplexer.addProcessor(stage);
			table = new Table();
			table.setFillParent(true);

			stage.addActor(table);
			//stage.setDebugAll(true);
			hudModel.hudComponent.stage = stage;
			return this;
		}

		public Builder withProgressBar() {
			Pixmap white = new Pixmap((int) System.densityIndependent(PROGRESSBAR_WIDTH), 1, Pixmap.Format.RGBA8888);
			white.setColor(Color.WHITE);
			white.fill();

			Pixmap green = new Pixmap((int) System.densityIndependent(PROGRESSBAR_WIDTH), 1, Pixmap.Format.RGBA8888);
			green.setColor(Color.GREEN);
			green.fill();

			ProgressBar.ProgressBarStyle progressBarStyle = new ProgressBar.ProgressBarStyle();
			progressBarStyle.background = new Image(new Texture(white)).getDrawable();
			progressBarStyle.knob = new Image(new Texture(green)).getDrawable();
			progressBarStyle.knobBefore = progressBarStyle.knob;

			ProgressBar progressBar = new ProgressBar(0, 100, 1, true, progressBarStyle);
			table.add(progressBar).expand().right().top().padRight(System.densityIndependent(10))
					.padTop(System.densityIndependent(10)).height(System.densityIndependent(50));
			table.row();
			hudModel.hudComponent.fuelMeter = progressBar;
			return this;
		}

		public Builder withVirtualJoystick() {
			Touchpad.TouchpadStyle style = new Touchpad.TouchpadStyle();
			style.background = new Image(new Texture("uiSkin/touchBackground.png")).getDrawable();
			style.knob = new Image(new Texture("uiSkin/touchKnob.png")).getDrawable();
			style.knob.setMinWidth(System.densityIndependent(50));
			style.knob.setMinHeight(System.densityIndependent(50));

			Touchpad touchpad = new Touchpad(System.densityIndependent(10), style);
			hudModel.hudComponent.touchpad = touchpad;
			table.add(touchpad).expandX().right().width(System.densityIndependent(120)).height(System.densityIndependent(120));
			return this;
		}

		@Override
		public Entity build() {
			return hudModel.createEntity();
		}
	}

}
