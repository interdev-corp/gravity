package com.interdev.gravity.entity.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.interdev.gravity.entity.component.PhysicsComponent;
import com.interdev.gravity.entity.component.PlanetaryGravityComponent;

public class PhysicsGravitySystem extends AbstractPhysicsSystem {

	private static final float GRAVITATIONAL_CONSTANT = 13000;

	private ImmutableArray<Entity> planetEntities;

	private static final ComponentMapper<PhysicsComponent> physicsComponentMapper = ComponentMapper.getFor(PhysicsComponent.class);
	private static final ComponentMapper<PlanetaryGravityComponent> planetaryGravityComponentMapper = ComponentMapper.getFor(PlanetaryGravityComponent.class);

	public PhysicsGravitySystem(World world) {
		super(world);
	}

	@Override
	public void addedToEngine(Engine engine) {
		planetEntities = engine.getEntitiesFor(Family.all(PhysicsComponent.class, PlanetaryGravityComponent.class).get());
	}

	@Override
	protected void updateInterval() {
		for (int i = planetEntities.size() - 1; i >= 0; i--) {
			Entity firstPlanet = planetEntities.get(i);
			for (int j = i - 1; j >= 0; j--) {
				Entity secondPlanet = planetEntities.get(j);
				calculateGravitationForce(firstPlanet, secondPlanet);
			}
		}
	}

	private void calculateGravitationForce(Entity firstPlanet, Entity secondPlanet) {
		Body firstBody = physicsComponentMapper.get(firstPlanet).body;
		PlanetaryGravityComponent firstGravityComponent = planetaryGravityComponentMapper.get(firstPlanet);

		Body secondBody = physicsComponentMapper.get(secondPlanet).body;
		PlanetaryGravityComponent secondGravityComponent = planetaryGravityComponentMapper.get(secondPlanet);

		Vector2 firstToSecondVec = firstBody.getPosition().cpy().sub(secondBody.getPosition());
		if (isInGravityField(firstToSecondVec.len(), firstGravityComponent.gravityFieldRadius + secondGravityComponent.gravityFieldRadius)) {
			float gravityForce = (GRAVITATIONAL_CONSTANT * firstBody.getMass() * secondBody.getMass()) / firstToSecondVec.len2();
			if (firstGravityComponent.fixed) {
				secondBody.applyForceToCenter(firstToSecondVec.nor().scl(gravityForce), true);
			} else {
				firstBody.applyForceToCenter(firstToSecondVec.scl(-1).nor().scl(gravityForce), true);
			}
		}
	}

	private boolean isInGravityField(float planetaryDistance, float fieldRadiusDistance) {
		return planetaryDistance < fieldRadiusDistance;
	}
}
