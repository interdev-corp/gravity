package com.interdev.gravity.entity.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.World;

import java.util.ArrayList;
import java.util.List;

public class EntitySystemManager {

	private List<EntitySystem> entitySystems = new ArrayList<>();

	public EntitySystemManager(SpriteBatch batch, World world, InputMultiplexer multiplexer) {
		addSystem(new PhysicsSteppingSystem(world));
		addSystem(new PlayerControlSystem());
		addSystem(new PhysicsGravitySystem(world));
		addSystem(new BackgroundRenderingSystem(batch));
		addSystem(new EntityRenderingSystem(batch, multiplexer));
		addSystem(new HUDRenderingSystem());
		addSystem(new PhysicsDebugRenderingSystem(world));
	}

	public void registerEntitySystems(Engine engine) {
		for (EntitySystem system : entitySystems) {
			engine.addSystem(system);
		}
	}

	private void addSystem(EntitySystem entitySystem) {
		entitySystem.priority = entitySystems.size();
		entitySystems.add(entitySystem);
	}
}
