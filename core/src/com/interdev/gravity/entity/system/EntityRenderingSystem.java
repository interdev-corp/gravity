package com.interdev.gravity.entity.system;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.interdev.gravity.entity.component.ParticleComponent;
import com.interdev.gravity.entity.component.PhysicsComponent;
import com.interdev.gravity.entity.component.SpriteComponent;
import com.interdev.gravity.entity.model.WorldModel;
import com.interdev.gravity.listener.ViewportZoomListener;

public class EntityRenderingSystem extends EntitySystem {

	private ImmutableArray<Entity> physicsEntities;

	private static final ComponentMapper<PhysicsComponent> physicsComponentMapper = ComponentMapper.getFor(PhysicsComponent.class);
	private static final ComponentMapper<SpriteComponent> spriteComponentMapper = ComponentMapper.getFor(SpriteComponent.class);
	private static final ComponentMapper<ParticleComponent> particleComponentMapper = ComponentMapper.getFor(ParticleComponent.class);

	private SpriteBatch spriteBatch;
	private Camera camera;
	private Viewport viewport;

	public EntityRenderingSystem(SpriteBatch batch, InputMultiplexer inputMultiplexer) {
		this.spriteBatch = batch;
		camera = new OrthographicCamera();
		viewport = new ExtendViewport(WorldModel.WORLD_WIDTH, WorldModel.WORLD_HEIGHT, camera);
		viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
		inputMultiplexer.addProcessor(new GestureDetector(new ViewportZoomListener(viewport)));
	}

	@Override
	public void addedToEngine(Engine engine) {
		physicsEntities = engine.getEntitiesFor(Family.all(PhysicsComponent.class, SpriteComponent.class).get());
	}

	@Override
	public void update(float deltaTime) {
		camera.update();
		spriteBatch.setProjectionMatrix(camera.combined);
		spriteBatch.begin();
		for (Entity entity : physicsEntities) {
			PhysicsComponent physicsComponent = physicsComponentMapper.get(entity);
			SpriteComponent spriteComponent = spriteComponentMapper.get(entity);
			updateEntity(physicsComponent, spriteComponent);
			spriteComponent.sprite.draw(spriteBatch);
			ParticleComponent particleComponent = particleComponentMapper.get(entity);
			if (particleComponent != null && particleComponent.active) {
				renderParticle(particleComponent, physicsComponent, spriteComponent, deltaTime);
			}
		}
		spriteBatch.end();
	}

	private void renderParticle(ParticleComponent particleComponent, PhysicsComponent physicsComponent, SpriteComponent spriteComponent, float deltaTime) {
		Body body = physicsComponent.body;
		Sprite sprite = spriteComponent.sprite;
		ParticleEffect particleEffect = particleComponent.particleEffect;
		ParticleEmitter emitter = particleEffect.getEmitters().first();
		Vector2 worldPos = body.getWorldPoint(new Vector2(-sprite.getWidth() / 2, 0));
		emitter.setPosition(worldPos.x, worldPos.y);
		rotateBy(body.getAngle() * MathUtils.radiansToDegrees + 180, emitter);
		particleComponent.particleEffect.draw(spriteBatch, deltaTime);
	}

	public void rotateBy(float angle, ParticleEmitter particleEmitter) {
		ParticleEmitter.ScaledNumericValue val = particleEmitter.getAngle();
		float amplitude = (val.getHighMax() - val.getHighMin()) / 2f;
		float h1 = angle + amplitude;
		float h2 = angle - amplitude;
		val.setHigh(h1, h2);
		val.setLow(angle);
	}

	private void updateEntity(PhysicsComponent physicsComponent, SpriteComponent spriteComponent) {
		Sprite sprite = spriteComponent.sprite;
		Body body = physicsComponent.body;
		sprite.setPosition(body.getPosition().x - sprite.getWidth() / 2, body.getPosition().y - sprite.getHeight() / 2);
		sprite.setRotation(MathUtils.radiansToDegrees * body.getAngle());
	}

}
