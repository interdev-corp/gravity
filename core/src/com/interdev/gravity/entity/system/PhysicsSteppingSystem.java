package com.interdev.gravity.entity.system;

import com.badlogic.gdx.physics.box2d.World;

public class PhysicsSteppingSystem extends AbstractPhysicsSystem {

	public PhysicsSteppingSystem(World world) {
		super(world);
	}

	@Override
	public void updateInterval() {
		world.step(TIME_STEP, 10, 8);
		world.clearForces();
	}

}
