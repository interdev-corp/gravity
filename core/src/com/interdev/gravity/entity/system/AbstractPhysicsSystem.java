package com.interdev.gravity.entity.system;


import com.badlogic.ashley.systems.IntervalSystem;
import com.badlogic.gdx.physics.box2d.World;

public abstract class AbstractPhysicsSystem extends IntervalSystem {

	protected static final float TIME_STEP = 1 / 60f;

	protected World world;

	public AbstractPhysicsSystem(World world) {
		super(TIME_STEP);
		this.world = world;
	}

	public AbstractPhysicsSystem() {
		super(TIME_STEP);
	}
}
