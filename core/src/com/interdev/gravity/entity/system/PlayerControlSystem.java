package com.interdev.gravity.entity.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.interdev.gravity.entity.component.HUDComponent;
import com.interdev.gravity.entity.component.ParticleComponent;
import com.interdev.gravity.entity.component.PhysicsComponent;
import com.interdev.gravity.entity.component.SpaceShipComponent;

public class PlayerControlSystem extends AbstractPhysicsSystem {

	private static final float PERCENTAGE_TO_FORCE_RATIO = 10;
	private static final float PERCENTAGE_TO_FUEL_WASTE_RATIO = 0.2f;

	private Entity shipEntity;
	private Entity hudEntity;

	private static final ComponentMapper<HUDComponent> hudComponentMapper = ComponentMapper.getFor(HUDComponent.class);
	private static final ComponentMapper<PhysicsComponent> physicsComponentMapper = ComponentMapper.getFor(PhysicsComponent.class);
	private static final ComponentMapper<SpaceShipComponent> spaceShipComponentMapper = ComponentMapper.getFor(SpaceShipComponent.class);
	private static final ComponentMapper<ParticleComponent> particleComponentMapper = ComponentMapper.getFor(ParticleComponent.class);

	@Override
	public void addedToEngine(Engine engine) {
		shipEntity = engine.getEntitiesFor(Family.one(SpaceShipComponent.class).get()).first();
		hudEntity = engine.getEntitiesFor(Family.one(HUDComponent.class).get()).first();
	}

	@Override
	protected void updateInterval() {
		Touchpad touchpad = hudComponentMapper.get(hudEntity).touchpad;
		Body body = physicsComponentMapper.get(shipEntity).body;
		SpaceShipComponent spaceShipComponent = spaceShipComponentMapper.get(shipEntity);
		ParticleComponent particleComponent = particleComponentMapper.get(shipEntity);

		Vector2 percentage = new Vector2(touchpad.getKnobPercentX(), touchpad.getKnobPercentY());
		if (!percentage.isZero() && spaceShipComponent.fuel > 0) {
			body.setTransform(body.getPosition(), MathUtils.degreesToRadians * percentage.angle());
			body.applyForceToCenter(percentage.cpy().scl(PERCENTAGE_TO_FORCE_RATIO), true);
			spaceShipComponent.fuel -= percentage.cpy().scl(PERCENTAGE_TO_FUEL_WASTE_RATIO).len();
			particleComponent.active = true;
		} else {
			particleComponent.active = false;
		}
	}
}
