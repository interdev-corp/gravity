package com.interdev.gravity.entity.system;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.interdev.gravity.entity.component.BackgroundComponent;

public class BackgroundRenderingSystem extends EntitySystem {

	private ImmutableArray<Entity> backgroundEntities;
	private static final ComponentMapper<BackgroundComponent> backgroundComponentMapper = ComponentMapper.getFor(BackgroundComponent.class);

	private SpriteBatch batch;
	private Camera camera = new OrthographicCamera();
	private Viewport viewport = new ScreenViewport(camera);

	public BackgroundRenderingSystem(SpriteBatch batch) {
		this.batch = batch;
	}

	@Override
	public void addedToEngine(Engine engine) {
		backgroundEntities = engine.getEntitiesFor(Family.all(BackgroundComponent.class).get());
	}

	@Override
	public void update(float deltaTime) {
		switchViewport();
		Entity backgroundEntity = backgroundEntities.first();
		BackgroundComponent backgroundComponent = backgroundComponentMapper.get(backgroundEntity);
		TiledDrawable td = new TiledDrawable(new TextureRegion(backgroundComponent.texture));
		batch.begin();
		td.draw(batch, 0, 0, viewport.getScreenWidth(), viewport.getScreenHeight());
		batch.end();
	}

	private void switchViewport() {
		viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
		batch.setProjectionMatrix(camera.combined);
	}
}