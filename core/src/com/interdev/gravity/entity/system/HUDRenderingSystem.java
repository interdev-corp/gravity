package com.interdev.gravity.entity.system;


import com.badlogic.ashley.core.*;
import com.interdev.gravity.entity.component.HUDComponent;
import com.interdev.gravity.entity.component.SpaceShipComponent;

public class HUDRenderingSystem extends EntitySystem {

	private Entity hudEntity;
	private Entity spaceShipEntity;

	private static final ComponentMapper<HUDComponent> hudEntityMapper = ComponentMapper.getFor(HUDComponent.class);
	private static final ComponentMapper<SpaceShipComponent> spaceShipComponentMapper = ComponentMapper.getFor(SpaceShipComponent.class);

	public HUDRenderingSystem() {
	}

	@Override
	public void addedToEngine(Engine engine) {
		hudEntity = engine.getEntitiesFor(Family.one(HUDComponent.class).get()).first();
		spaceShipEntity = engine.getEntitiesFor(Family.one(SpaceShipComponent.class).get()).first();
	}

	@Override
	public void update(float deltaTime) {
		HUDComponent hudComponent = hudEntityMapper.get(hudEntity);
		hudComponent.fuelMeter.setValue(spaceShipComponentMapper.get(spaceShipEntity).fuel);
		hudComponent.stage.act(deltaTime);
		hudComponent.stage.draw();

	}

}
