package com.interdev.gravity.entity.system;

import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.interdev.gravity.entity.model.WorldModel;

public class PhysicsDebugRenderingSystem extends EntitySystem {

	private Box2DDebugRenderer renderer;
	private World world;
	private Camera camera;

	public PhysicsDebugRenderingSystem(World world) {
		this.world = world;
		this.camera = new OrthographicCamera();
		Viewport viewport = new ExtendViewport(WorldModel.WORLD_WIDTH, WorldModel.WORLD_HEIGHT, camera);
		viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
		renderer = new Box2DDebugRenderer();
	}

	@Override
	public void update(float deltaTime) {
		renderer.render(world, camera.combined);
	}

}
