package com.interdev.gravity.entity.component;

import com.badlogic.ashley.core.Component;

public class PlanetaryGravityComponent implements Component {

	public float gravityFieldRadius;
	public boolean fixed;
}
