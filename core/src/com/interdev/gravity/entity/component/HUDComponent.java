package com.interdev.gravity.entity.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;

public class HUDComponent implements Component {

	public Stage stage;
	public Touchpad touchpad;
	public ProgressBar fuelMeter;
}
