package com.interdev.gravity.entity.component;


import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;

public class ParticleComponent implements Component {

	public ParticleEffect particleEffect;
	public boolean active;
}
