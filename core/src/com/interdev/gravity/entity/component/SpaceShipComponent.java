package com.interdev.gravity.entity.component;

import com.badlogic.ashley.core.Component;

public class SpaceShipComponent implements Component {

	public float fuel;
	public float thrust;
}
