package com.interdev.gravity.listener;

import com.badlogic.gdx.physics.box2d.Contact;
import com.interdev.gravity.GravityGame;
import com.interdev.gravity.entity.model.PlanetModel;
import com.interdev.gravity.entity.model.SpaceShipModel;
import com.interdev.gravity.screen.menu.IngameModalScreen;

public class ContactListener extends ContactListenerAdapter {

	private GravityGame game;

	public ContactListener(GravityGame game) {
		this.game = game;
	}

	@Override
	public void beginContact(Contact contact) {
		boolean win;
		Class first = (Class) contact.getFixtureA().getBody().getUserData();
		Class second = (Class) contact.getFixtureB().getBody().getUserData();
		if (first.equals(SpaceShipModel.class) && second.equals(PlanetModel.class)
				|| first.equals(PlanetModel.class) && second.equals(SpaceShipModel.class)) {
			win = false;
		} else {
			win = true;
		}
		game.setScreen(new IngameModalScreen(game, win));
	}

	@Override
	public void endContact(Contact contact) {

	}

}
