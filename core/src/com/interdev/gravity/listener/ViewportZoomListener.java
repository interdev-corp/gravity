package com.interdev.gravity.listener;

import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.utils.viewport.Viewport;

public class ViewportZoomListener extends GestureDetector.GestureAdapter {

	private static final float ZOOM_RATIO = 1;

	private Viewport viewport;
	private float worldWidth;
	private float worldHeight;

	public ViewportZoomListener(Viewport viewport) {
		this.viewport = viewport;
		this.worldWidth = viewport.getWorldWidth();
		this.worldHeight = viewport.getWorldHeight();
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		float ratio = initialDistance / distance * ZOOM_RATIO;
		viewport.setWorldWidth(worldWidth * ratio);
		viewport.setWorldHeight(worldHeight * ratio);
		viewport.apply();
		return true;
	}
}
