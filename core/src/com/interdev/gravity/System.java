package com.interdev.gravity;

import com.badlogic.gdx.Gdx;

public class System {

	public static void info(String msg) {
		Gdx.app.log("INFO", msg);
	}

	public static float densityIndependent(float size) {
		return size * Gdx.graphics.getDensity();
	}
}
