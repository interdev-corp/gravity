package com.interdev.gravity.screen.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.interdev.gravity.GravityGame;
import com.interdev.gravity.System;
import com.interdev.gravity.screen.level.LevelScreen;

public class IngameModalScreen extends AbstractMenuScreen {

	private static final String GAME_LOST_TEXT = "You have lost the game, do you wish to try again ?";
	private static final String GAME_WIN_TEXT = "You have won the game, do you wish to try again ?";

	private enum Options {
		TRY_AGAIN, GO_TO_MENU;
	}

	public IngameModalScreen(GravityGame game, boolean win) {
		super(game);
		initGameModal(win ? GAME_WIN_TEXT : GAME_LOST_TEXT);
	}

	private void initGameModal(String text) {
		Skin skin = getMenuSkin(FONT_SIZE_MEDIUM);
		Dialog dialog = new Dialog("", skin, "dialog") {
			@Override
			public void result(Object result) {
				if (Options.GO_TO_MENU.equals(result)) {
					game.setScreen(new MainMenuScreen(game));
				} else if (Options.TRY_AGAIN.equals(result)) {
					game.setScreen(new LevelScreen(game));
				}
			}
		};
		Label label = new Label(text, skin);
		label.setWrap(true);
		dialog.getContentTable().add(label).prefWidth(Gdx.graphics.getWidth() * 0.9f).padBottom(System.densityIndependent(10));
		TextButton tryAgainBtn = new TextButton("Try again", skin);
		TextButton goToMenuBtn = new TextButton("Main menu", skin);
		dialog.button(tryAgainBtn, Options.TRY_AGAIN);
		dialog.button(goToMenuBtn, Options.GO_TO_MENU);
		dialog.key(Input.Keys.ENTER, true);
		dialog.show(stage);
	}

}
