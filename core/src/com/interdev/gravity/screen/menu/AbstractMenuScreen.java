package com.interdev.gravity.screen.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.interdev.gravity.GravityGame;
import com.interdev.gravity.System;

public class AbstractMenuScreen extends ScreenAdapter {

	protected static final int FONT_SIZE_HIGH = 35;
	protected static final int FONT_SIZE_MEDIUM = 25;
	private static final String FONT_NAME = "default-font";

	protected GravityGame game;
	protected Stage stage;

	protected AbstractMenuScreen(GravityGame game) {
		this.game = game;
		stage = new Stage(new ScreenViewport(), game.getBatch());
		//stage.setDebugAll(true);
		Gdx.input.setInputProcessor(stage);
	}

	protected Skin getMenuSkin(int fontSize) {
		Skin skin = new Skin();
		skin.addRegions(new TextureAtlas(Gdx.files.internal("uiSkin/uiskin.atlas")));
		skin.add(FONT_NAME, generateFont(fontSize));
		skin.load(Gdx.files.internal("uiSkin/uiskin.json"));
		return skin;
	}

	protected BitmapFont generateFont(int fontSize) {
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Sansation-Regular.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = (int) (System.densityIndependent(fontSize));
		BitmapFont bitmapFont = generator.generateFont(parameter);
		generator.dispose();
		return bitmapFont;
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}
