package com.interdev.gravity.screen.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.interdev.gravity.GravityGame;
import com.interdev.gravity.screen.level.LevelScreen;

public class MainMenuScreen extends AbstractMenuScreen {

	public MainMenuScreen(GravityGame game) {
		super(game);
		initTable(getMenuSkin(FONT_SIZE_HIGH));
	}

	private void initTable(Skin skin) {
		Table table = new Table();
		stage.addActor(table);
		table.setFillParent(true);
		table.validate();
		table.top();
		table.pad(Value.percentWidth(.45F, table));
		initButtons(table, skin);
	}

	private void initButtons(Table table, Skin skin) {
		TextButton newGameBtn = new TextButton("New game", skin);
		newGameBtn.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreen(new LevelScreen(game));
				dispose();
			}
		});
		table.add(newGameBtn).width(Value.percentWidth(.75F, table)).pad(Value.percentHeight(.4F, newGameBtn));
		table.row();
		TextButton settingsBtn = new TextButton("Settings", skin);
		table.add(settingsBtn).width(Value.percentWidth(.75F, table)).pad(Value.percentHeight(.4F, settingsBtn));
		table.row();
		TextButton quitBtn = new TextButton("Quit", skin);
		quitBtn.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Gdx.app.exit();
			}
		});
		table.add(quitBtn).width(Value.percentWidth(.75F, table)).pad(Value.percentHeight(.4F, settingsBtn));
	}

}
