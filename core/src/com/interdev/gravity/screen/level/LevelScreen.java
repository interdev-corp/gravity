package com.interdev.gravity.screen.level;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.interdev.gravity.GravityGame;
import com.interdev.gravity.entity.system.EntitySystemManager;
import com.interdev.gravity.level.FirstLevel;
import com.interdev.gravity.level.GameLevel;
import com.interdev.gravity.listener.ContactListener;

public class LevelScreen extends ScreenAdapter {

	private GravityGame game;
	private SpriteBatch batch;
	private InputMultiplexer inputMultiplexer;

	private Engine engine;

	public LevelScreen(GravityGame game) {
		this.game = game;
		this.batch = game.getBatch();
		this.inputMultiplexer = new InputMultiplexer();
		Gdx.input.setInputProcessor(inputMultiplexer);
		createEngine();
	}

	private void createEngine() {
		engine = new Engine();
		GameLevel level = new FirstLevel(inputMultiplexer);
		level.registerEntities(engine);
		EntitySystemManager systemManager = new EntitySystemManager(batch, level.getGameWorld(), inputMultiplexer);
		systemManager.registerEntitySystems(engine);
		level.getGameWorld().setContactListener(new ContactListener(game));
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		engine.update(delta);
	}
}
